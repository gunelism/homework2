defmodule Takso.Repo.Migrations.UpdateBookingTable do
  use Ecto.Migration

  def change do
    alter table(:bookings) do
      add :distance, :integer
    end
  end
end
