# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Takso.Repo.insert!(%Takso.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Takso.{Repo, Accounts.User, Sales.Taxi, Sales.Booking, Sales.Allocation}
alias Ecto.Changeset

[%{name: "Fred Flintstone", username: "fred", password: "parool"}, # 1
 %{name: "Barney Rubble", username: "barney", password: "parool"}, # 2
 %{name: "Chingiz Mammadli", username: "mchingiz", password: "parool"}, # 3
 %{name: "Gunel Ismayilova", username: "gunelism", password: "parool"}, # 4
 %{name: "Ilyas Alizadeh", username: "ailyas", password: "parool"}, # 5
 %{name: "Hagigat Hasanova", username: "hhasanova", password: "parool"}] # +1
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)


[%{location: "Narva 25", username: "taxi_driver1", status: "busy"}, # 1
%{location: "Raatuse 22", username: "taxi_driver2", status: "available"}, # 2
%{location: "Raatuse 34", username: "taxi_driver3", status: "available"}, # 3
%{location: "Raatuse 56", username: "taxi_driver4", status: "available"}, # 4
%{location: "Raatuse 56", username: "taxi_driver5", status: "busy"}, # 5
%{location: "Narva 27", username: "taxi_driver6", status: "off-duty"}]
|> Enum.map(fn taxi_data -> Taxi.changeset(%Taxi{}, taxi_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%{pickup_address: "Fred Flintstone", dropoff_address: "fred", status: "cancelled", distance: 34, user_id: 3}, # 1
 %{pickup_address: "Fred Flintstone 2", dropoff_address: "fred 2", status: "rejected", distance: 34, user_id: 3}, # 2
 %{pickup_address: "Fred Flintstone 3", dropoff_address: "fred 3", status: "completed", distance: 34, user_id: 3}, # 3
 %{pickup_address: "Fred Flintstone 4", dropoff_address: "fred 4", status: "open", distance: 34, user_id: 3}, # 4
 %{pickup_address: "Fred Flintstone", dropoff_address: "fred", status: "cancelled", distance: 34, user_id: 4}, # 5
 %{pickup_address: "Fred Flintstone", dropoff_address: "fred", status: "open", distance: 34, user_id: 4}] # +1
|> Enum.map(fn booking_data -> Booking.changeset(%Booking{}, booking_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)


[%{status: "on-hold", taxi_id: 2, booking_id: 4}, # 1
%{status: "on-hold", taxi_id: 2, booking_id: 6}]
|> Enum.each(fn alloc_data -> Allocation.changeset(%Allocation{}, %{status: alloc_data.status})
                              |> Changeset.put_change(:booking_id, alloc_data.booking_id)
                              |> Changeset.put_change(:taxi_id, alloc_data.taxi_id)
                              |> Repo.insert! end)

# [%{location: "Narva 25", username: "taxi_driver1", status: "busy"}, # 1
# %{location: "Narva 27", username: "taxi_driver6", status: "off-duty"}]
# |> Enum.map(fn allocation_data -> Allocation.changeset(%Allocation{}, allocation_data) end)
# |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
