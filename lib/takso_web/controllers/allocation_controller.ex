defmodule TaksoWeb.AllocationController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias Takso.{Repo, Authentication}
  alias Ecto.{Changeset, Multi}
  alias Takso.Sales.{Taxi, Booking, Allocation}


  def index(conn, _params) do
    allocs = Repo.all(Allocation) |> Repo.preload([:taxi, :booking])
    render(conn, "index.html", allocs: allocs)
  end

  def accept(conn, %{"id" => id}) do
    user = Authentication.load_current_user(conn)
    allocation = Repo.get!(Allocation, id) |> Repo.preload([:taxi, :booking])
    booking = allocation.booking |> Repo.preload([:user])
    taxi = allocation.taxi

    # IO.puts "burdadi"
    IO.inspect user

    if allocation.taxi.username == user.username do
      Multi.new
      |> Multi.update(:allocation, Allocation.changeset(allocation, %{}) |> Changeset.put_change(:status, "allocated"))
      |> Multi.update(:taxi, Taxi.changeset(taxi, %{}) |> Changeset.put_change(:status, "busy"))
      |> Multi.update(:booking, Booking.changeset(booking, %{}) |> Changeset.put_change(:status, "accepted"))
      |> Repo.transaction()

      conn
      |> put_flash(:info, "You have accepted the booking")
      |> redirect(to: allocation_path(conn, :index))
    else
      conn
      |> put_flash(:error, "You are not permitted to accept this allocation")
      |> redirect(to: allocation_path(conn, :index))
    end


  end

  def reject(conn, %{"id" => id}) do
    render(conn, id)
  end
end
