defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Ecto.Changeset

  alias Takso.{Repo, Accounts.User}
  alias Takso.Sales.{Taxi, Booking, Allocation}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end

  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Takso.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Takso.Repo, {:shared, self()})
    %{}
  end

  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Takso.Repo)
    Hound.end_session
  end

  given_ ~r/^the following taxis are on duty$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn taxi -> Taxi.changeset(%Taxi{}, taxi) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to go from "(?<pickup_address>[^"]+)" to "(?<dropoff_address>[^"]+)"$/,
  fn state, %{pickup_address: pickup_address, dropoff_address: dropoff_address} ->
    {:ok, state |> Map.put(:pickup_address, pickup_address) |> Map.put(:dropoff_address, dropoff_address)}
  end

  and_ ~r/^I open STRS' web page$/, fn state ->
    navigate_to "/bookings/new"
    {:ok, state}
  end

  and_ ~r/^I enter the booking information$/, fn state ->
    fill_field({:id, "pickup_address"}, state[:pickup_address])
    fill_field({:id, "dropoff_address"}, state[:dropoff_address])
    {:ok, state}
  end

  when_ ~r/^I summit the booking request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    assert visible_in_page? ~r/Your taxi will arrive in \d+ minutes/
    {:ok, state}
  end

  then_ ~r/^I should receive a rejection message$/, fn state ->
    assert visible_in_page? ~r/At present, there is no taxi available!/
    {:ok, state}
  end

  # Allocation management by taxi

  given_ ~r/^the following users are present$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^the following taxis are present$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn taxi_data -> Taxi.changeset(%Taxi{}, taxi_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^the following bookings are present$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn booking_data -> Booking.changeset(%Booking{}, booking_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^the following allocations are present for current taxi$/, fn state, %{table_data: table} ->
    table
    |> Enum.each(fn alloc_data -> {booking_id, _} = Integer.parse(alloc_data.booking_id)
                                 {taxi_id, _} = Integer.parse(alloc_data.taxi_id)
                                 Allocation.changeset(%Allocation{}, %{status: alloc_data.status})
                                 |> Changeset.put_change(:booking_id, booking_id)
                                 |> Changeset.put_change(:taxi_id, taxi_id)
                                 |> Repo.insert! end)
    # |> Enum.map(fn alloc_data -> {booking_id, _} = Integer.parse(alloc_data.booking_id)
    #                              {taxi_id, _} = Integer.parse(alloc_data.taxi_id)
    #                              Allocation.changeset(%Allocation{}, %{status: alloc_data.status})
    #                              |> Changeset.put_change(:booking_id, booking_id)
    #                              |> Changeset.put_change(:taxi_id, taxi_id) end)
    # |> Enum.map(fn alloc_data -> Allocation.changeset(%Allocation{}, alloc_data) end)
    # |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to accept booking with ID of "(?<booking_id>[^"]+)"$/,
  fn state, %{booking_id: booking_id} ->
    {:ok, state |> Map.put(:booking_id, booking_id) }
  end

  and_ ~r/^I open allocations page$/, fn state ->
    navigate_to "/allocations"
    {:ok, state}
  end

  when_ ~r/^I click "(?<button_id>[^"]+)" button in front of specific allocation$/,
  fn state, %{button_id: button_id} ->
    click({:id, button_id})
    {:ok, state}
  end

  then_ ~r/^status of booking should change from "(?<argument_one>[^"]+)" to "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end

  and_ ~r/^status of allocation should change from "(?<argument_one>[^"]+)" to "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end

  and_ ~r/^status of my taxi should change from "(?<argument_one>[^"]+)" to "(?<argument_two>[^"]+)"$/,
  fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
    {:ok, state}
  end
end
