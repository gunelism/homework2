Feature: Taxi booking
  As a taxi driver
  I want to manage my bookings

  Scenario: Accepting allocation via web page
    Given the following users are present
          | name    | username  | 
          | Chingiz | mchingiz  | 
          | Gunel   | gunelism  |
          | Hagigat | hhasanova |
          | Ilyas   | taxi1     |
          | Tofiq   | taxi2     |
          | Nermin  | taxi3     |
    And the following taxis are present
          | username | location | status    |
          | taxi1    | Narva 23 | busy      |
          | taxi2    | Narva 25 | available |
          | taxi3    | Narva 27 | available |
    And the following bookings are present
          | pickup_address | dropoff_address | user_id | status    | distance |
          | Raatuse 22     | Liivi 2         | 1       | cancelled | 34       |
          | Raatuse 24     | Liivi 4         | 1       | rejected  | 45       |
          | Raatuse 26     | Liivi 6         | 1       | completed | 23       |
          | Raatuse 28     | Liivi 8         | 1       | open      | 86       |
          | Raatuse 30     | Liivi 9         | 2       | open      | 2        |
    And the following allocations are present for current taxi
          | status   | taxi_id | booking_id |
          | on-hold  | 2       | 4          |
          | on-hold  | 2       | 5          |
    And I want to accept booking with ID of "x"
    And I open allocations page
    When I click "accept2" button in front of specific allocation
    Then status of booking should change from "open" to "accepted"
    And status of allocation should change from "on-hold" to "allocated"
    And status of my taxi should change from "available" to "busy"